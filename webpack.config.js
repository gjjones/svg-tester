module.exports = {
	entry: './public/scripts/main.js',
	output: {
		filename: 'build/bundle.js'
	},
  debug: true,
  devtool: 'source-map',
	module: {
    // preLoaders: [
    //   { test: /\.js$/, loader: 'eslint-loader', exclude: /node_modules/ }
    // ],
		loaders: [
      { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ }
		]
	}
}
