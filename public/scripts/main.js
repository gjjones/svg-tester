import fileParser from './fileParser';
import {
  dragOverHandler,
  fileSelectHandlerBuilder
} from './dropHandlers';

let fileSelectHandler = fileSelectHandlerBuilder(fileParser);

var dropEl = document.getElementById('drop_zone');
dropEl.addEventListener('dragover', dragOverHandler, false);
dropEl.addEventListener('drop', fileSelectHandler, false);
