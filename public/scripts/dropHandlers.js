export function fileSelectHandlerBuilder(fileParser) {
  return function fileSelectHandler(evt) {
    evt.stopPropagation();
    evt.preventDefault();

    const files = evt.dataTransfer.files;
    for (let i = 0, file = files[i]; file; i++, file = files[i]) {
      let reader = new FileReader();
      reader.onload = function(e) {
        fileParser(e.target.result);
      };
      reader.readAsText(file);
    }
  }
}

export function dragOverHandler(evt) {
  evt.stopPropagation();
  evt.preventDefault();
  evt.dataTransfer.dropEffect = 'copy';
}
